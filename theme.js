const baseFontSize = 30
const em2px = (size, base) => `${size * base}px`

export const fontSizes = {
  base: em2px(1, baseFontSize),
  twoTimes: em2px(2, baseFontSize),
  threeTimes: em2px(3, baseFontSize),
  fourtTimes: em2px(4, baseFontSize),
}

export const colors = {
  mainRed: '#da2037',
  mainOrange: '#f58345',
  mainBlack: '#29172e',
}

const fonts = {
  main: '',
  alternative: ''
}

export const gradients = {
  main: {
    bottom: `linear-gradient(to bottom, ${colors.mainRed}, ${colors.mainOrange})`,
    top: `linear-gradient(to top, ${colors.mainRed}, ${colors.mainOrange})`,
    left: `linear-gradient(to left, ${colors.mainRed}, ${colors.mainOrange})`,
    right: `linear-gradient(to right, ${colors.mainRed}, ${colors.mainOrange})`,
  }
}

export default {
  googleFont: 'https://fonts.googleapis.com/css?family=Fira+Mono|Fira+Sans:400,600&display=swap',
  font: 'Fira Sans, Times New Roman',
  colors: {
    text: colors.mainBlack,
    background: 'white'
  },
  p: {
    fontSize: em2px(1, baseFontSize),
    marginTop: '0',
    marginBottom: '.5em',
    alignSelf: 'flex-start',
    lineHeight: '1.5em;'
  },
  h1: {
    color: colors.mainRed,
    fontSize: em2px(3, baseFontSize),
    marginTop: '0',
    marginBottom: '.25em',
    alignSelf: 'flex-start',
    lineHeight: '1.25em;'
  },
  h2: {
    fontSize: em2px(2.5, baseFontSize),
    marginTop: '0',
    marginBottom: '.5em',
    alignSelf: 'flex-start',
    lineHeight: '1.25em;'
  },
  h3: {
    fontSize: em2px(2, baseFontSize),
    marginTop: '0',
    marginBottom: '.5em',
    alignSelf: 'flex-start',
    lineHeight: '1.25em;'
  },
  h4: {
    fontSize: em2px(1, baseFontSize),
    marginTop: '0',
    marginBottom: '.5em',
    alignSelf: 'flex-start',
    lineHeight: '1.25em;'
  }
}