import React from 'react'

import theme, { colors, gradients } from '../theme'

export default ({ background, children }) => (
  <div
    style={{
      width: '100vw',
      padding: '0 12.5%',
      height: '100vh',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center',
      textAlign: 'left',
      paddingTop: '1em',
      position: 'relative',
      background: background || theme.colors.background
    }}>
    {children}
  </div>
)