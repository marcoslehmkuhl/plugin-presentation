import React from 'react'

import { colors, gradients } from '../theme'

export default ({ children, style, alternative }) => (
  <div
    style={{
      width: '100vw',
      padding: '0 12.5%',
      height: '100vh',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',  
      background: alternative ? 'black' : gradients.main.top,
      color: alternative ? 'white' : colors.mainRed,
    }}>
    {children}
  </div>
)