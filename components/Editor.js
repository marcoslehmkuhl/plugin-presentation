import React from 'react'
import styled from 'styled-components'

const Box = styled.div`
  background-color: white;
  width: 600px;
  height: 350px;
  box-shadow: #8e8e8e 1px 3px 7px 0px;
  border-radius: 5px;
  align-self: flex-start;
`

const Header = styled.div`
  height: 60px;
  border-bottom: 1px solid #e2e2e2;
  padding: 0 16px;
  display: flex;
  align-items: center;
`

const Key = styled.div`
  font-family: 'Roboto Mono';
  display: inline-block;
  font-size: 28px;
  width: 40px;
  text-align: center;
  color: #252525;
`

const Content = styled.div`
  padding: 24px;
`

const Heading = styled.p`
  font-size: 36px !important;
  font-family: 'Times New Roman';
`

const Paragraph = styled.p`
  font-size: 16px !important;
  font-family: 'Times New Roman';
`

const Editor = () => (
  <Box>
    <Header>
      <Key>B</Key>
      <Key>I</Key>
      <Key>U</Key>
    </Header>
    <Content>
      <Heading>Article Title</Heading>
      <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec iaculis sem, at vehicula turpis. Mauris tincidunt dignissim orci sed luctus. In elementum dictum ex, a volutpat massa scelerisque ac. Cras vitae luctus lacus. In id odio vitae mauris convallis pretium sit amet vel nibh. Nam et cursus dui. Morbi dictum </Paragraph>
    </Content>
  </Box>
)

const NewKey = styled(Key)`
  background-color: white;
  width: auto;
  box-shadow: #8e8e8e 1px 3px 7px 0px;
  padding: 5px 36px 5px 16px;
  border-radius: 5px;
  position: absolute;
  top: 90px;
  &:after {
    content: '';
    position: absolute;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 8px 8px 0 8px;
    border-color: #007bff transparent transparent transparent;
    top: 20px;
    right: 10px;
  }
`

const Addon = () => <NewKey>Times New Roman</NewKey>

export { Addon }

export default Editor