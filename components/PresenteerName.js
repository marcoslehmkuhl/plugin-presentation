import React from 'react'
import { fontSizes } from '../theme'

const PresenteerName = ({ name, email }) => (
  <div
    style={{
      'fontSize': fontSizes.base,
      'color': 'white',
      'fontFamily': 'Fira Mono'
    }}
  >
    {name} | {email}
  </div>
)

export default PresenteerName