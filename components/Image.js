import React from 'react'

import theme, { colors, gradients } from '../theme'

export default ({ src, style, ...props }) => (
  <img
    style={{
      maxWidth: '100%',
      boxShadow: '#8e8e8e 1px 3px 7px 0px',
      ...style
    }}
    src={ src }
    { ...props }
  />
)