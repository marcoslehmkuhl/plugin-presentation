import React from 'react'

import theme, { colors, gradients } from '../theme'

export default ({ children, gutter }) => {
  const [a, ...rest] = React.Children.toArray(children)

  return (
    <div style={{ 
      display: 'flex', 
      flexDirection: 'row',
      width: '100%'
    }}>
      <div style={{ width: '50%', paddingRight: gutter ? '12px' : '0px' }}>{a}</div>
      <div style={{ width: '50%', paddingLeft: gutter ? '12px' : '0px' }}>{rest}</div>
    </div>
)}